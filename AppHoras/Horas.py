import os
import time
from tkinter import messagebox

class Usuario:

    def __init__(self, nombre, HorasContrato=48, PrecioHoras=4, SueldoNomina=400):
        self.nombre = str(nombre)
        self.HorasContrato = HorasContrato
        self.PrecioHoras = PrecioHoras
        self.SueldoNomina = SueldoNomina
        self.DiaSemana = time.strftime("%A")
        self.nombreMes = time.strftime("%B")


    def CrearUsuario(self):
        try:
            #Tendremos que cambiarlo en windows/Mac.
            #print(os.getcwd())
            os.chdir(os.getcwd())
            self.carpeta = os.mkdir(self.nombre)
            messagebox.showinfo("Informacion","Usuario Creado Correctamente")
        except FileExistsError:
            messagebox.showwarning("Informacion","El usuario existe, los datos se crearan en ella")

            
    def Horas(self, horas):
        global Horas

        try:
            self.Horas = horas
            if self.nombre != '' and self.Horas != '0.0':
                #Cambiamos en windows/Mac.
                #print(os.getcwd())
                rutaInicial = os.getcwd()
                #print(self.nombre)
                os.chdir(self.nombre)
                self.archivo = open("horas.txt","a+")
                lista1 = [self.Horas, "\n"]
                self.archivo.writelines(lista1)
                self.archivo.close()
                os.chdir(rutaInicial)
                messagebox.showinfo("Informacion","Las horas han sido añadidas correctamente")
            else:
                messagebox.showwarning("Informacion","El nombre no puede estar vacio")
        except FileNotFoundError:
                messagebox.showwarning("Informacion","El usuario no existe")


class CalculoHoras(Usuario):
    
    def VerHoras(self):
        try:
            lista = []
            #Cambiamos en windows/Mac
            rutaInicial = os.getcwd()
            os.chdir(os.getcwd() + "/" + self.nombre)
            self.leerArchivo = open("horas.txt","r")
            for lineaArchivo in self.leerArchivo:
                lineaArchivo = float(lineaArchivo)      #Convertimos a flotante los
                lista.append(lineaArchivo)              # numeros
            messagebox.showinfo("Informacion","Las horas de %s del mes de %s son: %s"%(self.nombre, self.nombreMes, sum(lista)))
                
            self.leerArchivo.close()
            os.chdir(rutaInicial)
        except FileNotFoundError:
            messagebox.showwarning("Informacion","El usuario no existe")


    def CalculoSueldo(self):        #Correccion del calculo del sueldo de menos de 48 horas
        try:
            lista = []
            rutaInicial = os.getcwd()
            os.chdir(self.nombre)
            self.calculoArchivo = open("horas.txt","r")
            for linea in self.calculoArchivo:
                linea = float(linea)
                lista.append(linea)
            self.totalHoras = sum(lista)

            if self.totalHoras <= self.HorasContrato:
                messagebox.showinfo("Informacion","El sueldo de %s del mes de %s es: %s"%(self.nombre, self.nombreMes, self.SueldoNomina))
            elif self.totalHoras > self.HorasContrato:
                self.sueldo = (self.totalHoras - self.HorasContrato) * self.PrecioHoras	+ self.SueldoNomina
                messagebox.showinfo("Informacion", "El sueldo de %s del mes de%s es: %s"%(self.nombre, self.nombreMes, self.sueldo))
            os.chdir(rutaInicial)


        except FileNotFoundError:
            messagebox.showwarning("Informacion","El usuario no existe")
		
		
			
		
		
	



            
    
