from Horas import Usuario, CalculoHoras
import tkinter as tk
from tkinter import messagebox
import os
#=====================VENTANA CREACION DE USUSARIO======================================

def AccionCrear():      # Funcion para crear un objeto de la clase Usuario
	global nombre      # y crear el usuario introducido en el Entry.
	nombre = str(entrada1.get())
	if nombre != '':
		obj = Usuario(nombre)
		obj.CrearUsuario()
	else:
		messagebox.showwarning("titulo","No se puede dejar en blanco el Nombre.")

def VentanaUsuario():     #Funcion de ventana Crear Usuario.
	'''Creacion de una nueva ventana, para crear usuarios.'''
	global entrada1, entrada2, entrada3
    
	ventanaUsuario = tk.Toplevel(ventana)
	ventanaUsuario.title("Ventana creacion usuario")
	ventanaUsuario.geometry("250x150")
    
	et1 = tk.Label(ventanaUsuario, text="Nombre:").place(x=20,y=20)
	
    
	entrada1 = tk.StringVar()
	#entrada2 = tk.IntVar()
	#entrada3 = tk.StringVar()

	entry1 = tk.Entry(ventanaUsuario, width=7, textvariable=entrada1).place(x=100,y=15)
	#entry2 = tk.Entry(ventanaUsuario, width=7, textvariable=entrada2).place(x=100,y=75)
	#entry3 = tk.Entry(ventanaUsuario, width=7, textvariable=entrada3).place(x=100,y=135)

	boton = tk.Button(ventanaUsuario, text="Crear", command=AccionCrear, width=10, bg="blue").place(x=70,y=100)
    
    
def AnadirHoras():      
	nombreU = str(nombreHoras.get())
	horasU = str(horas.get())
	obj = Usuario(nombreU)
	obj.Horas(horasU)
    

def VentanaHoras():
    global nombreHoras, horas
    ventanaHoras = tk.Toplevel(ventana)
    ventanaHoras.title("Ventana para añadir horas")
    ventanaHoras.geometry("250x250")
    et4 = tk.Label(ventanaHoras, text="Nombre:").place(x=20,y=50)
    et5 = tk.Label(ventanaHoras, text="Horas:").place(x=20,y=100)

    nombreHoras = tk.StringVar()
    horas = tk.DoubleVar()

    entry4 = tk.Entry(ventanaHoras, width=7, textvariable=nombreHoras).place(x=100,y=45)
    entry5 = tk.Entry(ventanaHoras, width=7, textvariable=horas).place(x=100,y=95)

    boton2 = tk.Button(ventanaHoras, text="Añadir",command=AnadirHoras, width=10,).place(x=70,y=200)
    
#=====================================VENTANA PARA VER LAS HORAS DEL MES=================================

def AccionVerHoras():
	global nombreverhoras
	nombreverhoras = str(entradateclado.get())
	obj = CalculoHoras(nombreverhoras)
	obj.VerHoras()



def VentanaVerHoras():      #Funcion ventana para ver las horas
	'''Creacion de la ventana para ver las horas de cada usuario'''
	global entradateclado
	entradateclado = tk.StringVar()

    
	ventanaverhoras = tk.Toplevel(ventana)
	ventanaverhoras.title("Ver Horas")
	ventanaverhoras.geometry("250x150")

	etiquetaverhoras = tk.Label(ventanaverhoras, text="Nombre:").place(x=20,y=20)

	entradaverhoras = tk.Entry(ventanaverhoras, textvariable=entradateclado, width=10).place(x=100,y=15)

	botonverhoras = tk.Button(ventanaverhoras, text="Buscar", command=AccionVerHoras, width=8).place(x=100,y=90)


#=============================VENTANA PARA CALCULAR SUELDO===========================================================
def AccionSueldo():
	global nombreSueldo
	nombreSueldo = nombreVentanaSueldo.get()
	obj = CalculoHoras(nombreSueldo)
	obj.CalculoSueldo()
	




def VentanaSueldo():
	'''Ventana de Calculo del sueldo del mes'''
	global nombreVentanaSueldo
	nombreVentanaSueldo = tk.StringVar()
	

	ventanaSueldo = tk.Toplevel(ventana)
	ventanaSueldo.title("Sueldo del mes")
	ventanaSueldo.geometry("210x150")
	
	etiquetaSueldo = tk.Label(ventanaSueldo, text="Nombre").place(x=20,y=20)
	entrySueldo = tk.Entry(ventanaSueldo,textvariable=nombreVentanaSueldo, width=10).place(x=100,y=15)

	botonSueldo = tk.Button(ventanaSueldo, text="Calcular", width=7, bg="green", command=AccionSueldo).place(x=100,y=50)
	
	




#==============================VENTANA PARA BORRAR USUARIO ==================================


def AccionBorrar():         #Podemos mejorar esto, creando una clase en el archivo Horas.py
    
	global nombreBorrar
    
	nombreBorrar = entradaBorrar.get()
    
	rutaCarpeta = os.getcwd() + "/" + nombreBorrar
	rutaPrincipal = os.getcwd()
	if os.path.exists(rutaCarpeta + "/horas.txt"):
		os.chdir(rutaCarpeta)
		os.remove("horas.txt")
		os.chdir(rutaPrincipal)
		os.rmdir(rutaCarpeta)
		messagebox.showinfo("Informacion","Carpeta Borrada con Exito")
	elif os.path.exists(rutaCarpeta):
		messagebox.showwarning("Informacion","El usuario no existe")           
		os.chdir(rutaPrincipal)
		os.rmdir(rutaCarpeta)
        
    
def VentanaBorrar():

	global entradaBorrar
	entradaBorrar = tk.StringVar()

	ventanaborrar = tk.Toplevel(ventana)
	ventanaborrar.title("Borrar Usuario")
	ventanaborrar.geometry("200x150")

	etiquetaborrar = tk.Label(ventanaborrar, text="Nombre:").place(x=20,y=20)
    
	etiquetaEntry = tk.Entry(ventanaborrar, textvariable=entradaBorrar, width=7).place(x=100,y=15)

	botonborrar = tk.Button(ventanaborrar, text="Borrar", width=5, command=AccionBorrar, bg="red").place(x=100,y=90)
   
    
    

#================================VENTANA PRINCIPAL========================================
ventana = tk.Tk()
ventana.title("App Horas")
ventana.geometry("490x300")

titulo = tk.Label(ventana, text="-------HORAS MUELLE-------").pack()
#Etiqueta y boton Creacion de usuario
et_usuario = tk.Label(ventana, text="Crear Usuario:").place(x=20,y=60)
btn_usuario = tk.Button(ventana, text="Ir",command=VentanaUsuario, width=3).place(x=150,y=55)

# Etiqueta y boton de borrar usuario
et2_usuario = tk.Label(ventana, text="Borrar Usuario:").place(x=20,y=120)
btn2_usuario = tk.Button(ventana, text="Ir",command=VentanaBorrar, width=3).place(x=150,y=115)

# Etiqueta y boton de Añadir horas
et3_usuario = tk.Label(ventana, text="Añadir horas:").place(x=250,y=60)
btn3_usuario = tk.Button(ventana, text="Ir",command=VentanaHoras, width=3).place(x=400,y=55)

# Etiqueta y boton de Ver horas
et4_usuario = tk.Label(ventana, text="Ver horas:").place(x=250,y=120)
btn4_usuario = tk.Button(ventana, text="Ir",command=VentanaVerHoras, width=3).place(x=400,y=115)

# Calculo de horas del mes
et5_usuario = tk.Label(ventana, text="Calculo de Sueldo:").place(x=250,y=180)
btn5_usuario = tk.Button(ventana, text="Ir", width=3, command=VentanaSueldo).place(x=400,y=175) 

ventana.mainloop()
#========================================================================================





